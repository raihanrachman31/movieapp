import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import { UserOutlined } from '@ant-design/icons';
import { Comment, Avatar, Rate, Form, Button, Input, Spin} from 'antd';
import  UserComment from '../components/UserComment';
import { submitReview, deleteReview,editReview } from '../store/actions/moviereview';
import { movieDetails } from '../store/actions/moviedetail';
import moment from 'moment';
import '../assets/styles/MovieView.scss';
import store from '../store/index';

const { TextArea } = Input;
const rateDesc =['1/10', '2/10', '3/10', '4/10', '5/10','6/10', '7/10', '8/10', '9/10', '10/10']

const CommentEditor = ({ setValue, onSubmit, submitting, reviewValue, rateChange, rateValue, closeEditor, editStatus}) => (
  <div className="editor">
    <Rate count={10} style={{fontSize:".8rem",display:"inline-block"}} tooltips={rateDesc} onChange={rateChange}/>
    {rateValue ? <span className="ant-rate-text">{rateDesc[rateValue - 1]}</span> : ''}
    <Form.Item>
      <TextArea autoFocus rows={4} onChange={e => setValue(e.target.value)} value={reviewValue}/>
    </Form.Item>
    <Form.Item>
      <Button style={{marginRight:8, width:80}} htmlType="submit" loading={submitting} onClick={onSubmit} type="primary">
        Post
      </Button>
      {editStatus && <Button style={{marginRight:8, width:80}} onClick={closeEditor} type="ghost">
        Cancel
      </Button>}
    </Form.Item>
  </div>
);

const CommentForm = () => {
  //store
  const isAuthenticate = useSelector( state => state.auth.isAuthenticate);
  const myComment = useSelector( state => state.movieDetail.myComment);

  //state
  const [value, setValue] = useState(myComment.description);
  const [rateValue, setRateValue] = useState(0);
  const [submitting,setSubmitting] = useState(false);
  const [isLoading,setIsLoading] = useState(false);
  const [editStatus,setEditStatus] = useState(false);
  const { id } = useParams();
  const dispatch = useDispatch();

  //function
  const handleSubmit = async e => {
    e.preventDefault();
    if (!value) {
      return;
    }
    setSubmitting(true);
    const data = { 
      description: value,
      rating: rateValue
    };
    !editStatus && await dispatch(submitReview(data,id));
    editStatus && await dispatch(editReview(data,id));
    await dispatch(movieDetails(id));
    const newReview = store.getState().movieDetail.myComment.description
    setSubmitting(false);
    setEditStatus(false);
    setValue(newReview);
    setRateValue(0);
  };

  const handleDelete = async() => {
    setIsLoading(true);
    await dispatch(deleteReview(id));
    await dispatch(movieDetails(id));
    setIsLoading(false);
  }

  useEffect( () => {
    setEditStatus(false);
  },[])

  useEffect( () => {
    if(editStatus === true && !!!myComment.description) {
      setEditStatus(false);
    }
  },[editStatus, myComment])
	
	//modal & component
	const displayComment = [{
    author: <span style={{textTransform:"capitalize"}}>{localStorage.getItem('userName')}</span>,
		avatar: 
			<div>
				<Avatar
					icon={<UserOutlined />}
					src={localStorage.getItem('userAvatar')}
					alt="Han Solo"
				/>
			</div>,
		content: 
			<>
        {editStatus &&
          <CommentEditor
            setValue={setValue}
            onSubmit={handleSubmit}
            submitting={submitting}
            reviewValue={value}
            rateValue={rateValue}
            rateChange={value=>setRateValue(value)}
            editStatus={editStatus}
            closeEditor={()=>setEditStatus(false)}
          />
        }
        {!editStatus &&
          <div>
            <Rate count={10} tooltips={rateDesc} style={{fontSize:".8rem",display:"inline"}} disabled defaultValue={myComment.rating}/>
            {!!myComment.rating ? <span className="ant-rate-text">{rateDesc[myComment.rating - 1]}</span> : ''}
            <p style={{display: "block", margin:0}} >{myComment.description}</p>
          </div>
        }
			</>,
		datetime: moment().format('YYYY-MM-DD HH:mm:ss')
	}]

  return(
    <section className="comment-form">
      <Spin spinning={isLoading}>
        {isAuthenticate && !!myComment.rating &&
          <UserComment
            displayComment={displayComment}
            handleDelete={handleDelete}
            toggleEdit={() => { setEditStatus(!editStatus) }}
          /> }
        {isAuthenticate && 
          <Comment
            avatar={!!!myComment.description &&
              <Avatar
              icon={<UserOutlined />}
              src={localStorage.getItem('userAvatar')}
              alt="Han Solo"
              />
            }
            content={!!!myComment.description &&
              <CommentEditor
                setValue={setValue}
                onSubmit={handleSubmit}
                submitting={submitting}
                reviewValue={value}
                rateValue={rateValue}
                rateChange={value=>setRateValue(value)}
                editStatus={editStatus}
                closeEditor={()=>setEditStatus(false)}
              /> 
            }
          />
        }
      </Spin>
    </section>
  )
}

export default CommentForm
