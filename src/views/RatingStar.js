import React from "react";
import { StarFilled} from '@ant-design/icons';
import {useSelector} from "react-redux";
import '../assets/styles/MovieView.scss';

const RatingStar = () => {
    const vote = useSelector(state => state.movieDetail.details.vote_count);
    const rating = useSelector(state => state.movieDetail.details.vote_average);
    return(
        <div className="review-wrapper">
            <div className="rating-wrapper">
            {[...Array(10)].map((star,i) => {
             const ratingValue = i +1;
            return(
                <label key={ratingValue}>
                    <input 
                    type="radio" 
                    name="rating" 
                    value={ratingValue}
                    />
                    <StarFilled 
                    className={ratingValue <= rating  ? "yellow-star": "grey-star"} size={100}
                    />
                </label>
            )
            })}
            </div>
                <span> {rating ? rating : 0 } of 10 </span>
                <span>({vote} Reviews)</span>
        </div>
    )
}

export default RatingStar;