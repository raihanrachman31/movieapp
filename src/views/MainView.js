import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Menu, Row, Col, Carousel, Pagination, Spin, Typography} from 'antd';
import { Card } from 'antd';
import { getMovies,filterMovieGenre, getMoviesBypage, searchMovie } from '../store/actions/movies'
import { Link } from 'react-router-dom'
import { DownOutlined } from '@ant-design/icons';
import '../assets/styles/MainView.scss'
import { TOGGLE_SEARCH_FINISHED } from '../store/actions/types';


const MainView = () => {
	//component
	const { Meta } = Card
	const { Text, Title } = Typography

	//state
	const carouselImage = [
		{
			id: 1,
			imageUrl:require('../assets/images/fight.jpg')
		},
		{
			id: 2,
			imageUrl:require('../assets/images/now.jpg')
		},
		{
			id: 3,
			imageUrl:require('../assets/images/fight.jpg')
		},
		{
			id: 4,
			imageUrl:require('../assets/images/now.jpg')
		}
	]
	const [ currentPage, setCurrentPage] = useState(1)
	const totalMovies = 10000 

	//store
	const movies = useSelector( state => state.movies.movies )
	const loading = useSelector( state => state.movies.loading )
	const genres = useSelector( state => state.movies.genres )
	const filterStatus = useSelector( state => state.movies.filterStatus)
	const searchStatus = useSelector( state => state.movies.searchStatus)
	const isSearch = useSelector ( state => state.movies.isSearch )
	const dispatch = useDispatch()

	//fucntion
	const pageChange = (page) => {
		setCurrentPage(page)
		console.log(filterStatus)
		if(filterStatus){
			dispatch(filterMovieGenre(filterStatus,page))
		} else if(searchStatus){
			dispatch(searchMovie(searchStatus,page))
		} else {
			dispatch(getMoviesBypage(page))
		}
	}

	const genreFilter = async (genre) => {
		setCurrentPage(1)
		dispatch(filterMovieGenre(genre,1))
	}

	const getAllMovies = async () => {
		await dispatch(getMovies())
		setCurrentPage(1)
	}

	const toggleSearch = () => {
    dispatch({type:TOGGLE_SEARCH_FINISHED})
  }

	const settingsCarousel = {
		className: "center",
		slidesToShow: 1,
		autoplay: true,
		effect:"fade",
		speed: 2000,
		autoplaySpeed:4000
	}

	const findGenre = (genre_ids) => {
		if(genres !== undefined){
			let movieGenres =[]
				for(let i = 0; i < genre_ids.length; i++) {
					let newGenre = genres.find( genre => genre.id === genre_ids[i])
					movieGenres = [...movieGenres, newGenre.name]
				}
			movieGenres = movieGenres.join(" ")
			return movieGenres
		}
	}
	
	return (
	<section className="main-view-wrapper">
		<DownOutlined onClick={toggleSearch} className={isSearch && !loading ? "close-icon animate-down" : " close-icon hidden"}/>

		<section className={isSearch ? "opacity-zero hero-wrapper" : "hero-wrapper"}>
			<Row className="hero-row" justify="center" align="middle">
				<Col span={12}>
					<Title strong="true" className="hero-title">Discus best movie at the best place.</Title>
					<Text className="hero-text">Search newest and popular movies on the fly. Review a movie in a daily basis. Join the vast community of movie admirers and professional movie reviewers.</Text>
				</Col>
				<Col span={12}>
					<Carousel id="main-carousel" {...settingsCarousel}>
					{carouselImage.map( carousel =>
						<div key={carousel.id}>
							<img style={{width:440, borderRadius:2,boxShadow: "0 3px 10px rgba(106, 107, 122, 0.2)",margin:"16px auto"}} src={carousel.imageUrl} alt="hm"/>
						</div>
					)}
					</Carousel>
				</Col>
			</Row>
		</section>
		
		<section style={isSearch ? {top: -530} : {}} className={`filter-wrapper`}>
			<Menu className="filter" style={{ padding:"0 24px", margin: "24px 0", marginTop:0}}theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
					<Menu.Item key="app" disabled>Browse by Category</Menu.Item>
					<Menu.Item onClick={getAllMovies} key="1">All</Menu.Item>
					<Menu.Item onClick={()=>genreFilter(28)} key="2">Action</Menu.Item>
					<Menu.Item onClick={()=>genreFilter(35)} key="3">Comedy</Menu.Item>
					<Menu.Item onClick={()=>genreFilter(80)} key="4">Crime</Menu.Item>
					<Menu.Item onClick={()=>genreFilter(18)} key="5">Drama</Menu.Item>
					<Menu.Item onClick={()=>genreFilter(14)} key="6">Fantasy</Menu.Item>
					<Menu.Item onClick={()=>genreFilter(36)} key="7">History</Menu.Item>
					<Menu.Item onClick={()=>genreFilter(27)} key="8">Horror</Menu.Item>
					<Menu.Item onClick={()=>genreFilter(9648)} key="9">Mystery</Menu.Item>
					<Menu.Item onClick={()=>genreFilter(10749)} key="10">Romance</Menu.Item>
				</Menu>
				
			<Row style={{maxWidth: "100%",margin: "0 24px"}} justify="space-around" >
				{!loading && movies.map( movie =>
				<Col key={movie.id} style={{margin:8}} flex="100px" >
					<Card
						hoverable
						cover={<Link to={`/${movie.title}/${movie.id}`}><img  className="main-movie-card" alt="example" style={{width:200}} src={"https://image.tmdb.org/t/p/w500"+movie.poster_path}/></Link>}
					>
						<Meta style={{height: 100,width:"150px"}} title={movie.title} description={findGenre(movie.genre_ids)} />
					</Card>,
				</Col> 
					)}
				{loading && <Spin style={{margin:"32px"}} size="large" />}
			</Row>
			<Pagination pageSize={20} showSizeChanger={false} current={currentPage} onChange={pageChange} total={totalMovies} />
		</section>	
	</section>
	)
}

export default MainView
