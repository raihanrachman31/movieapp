import React from "react";
import { useSelector } from 'react-redux'
import { Comment, List, Avatar, Rate, ConfigProvider} from 'antd';
import { UserOutlined } from '@ant-design/icons';
import moment from 'moment';
const rateDesc =['1/10', '2/10', '3/10', '4/10', '5/10','6/10', '7/10', '8/10', '9/10', '10/10']

const CommentList = ()=> {
  const storeAllReview = useSelector( state => state.movieDetail.comments)
  const allReview = storeAllReview.filter( review => review.userId !== parseInt(localStorage.getItem("userId")))

  const customizeRenderEmpty = () => (
    <div style={{ textAlign: 'center' }}>
      <p>No Review Yet</p>
    </div>
  );

    return(
      <div className="comment-lists">
        <ConfigProvider renderEmpty={customizeRenderEmpty}>
        {storeAllReview.length === 0 ? 
        <List
          className="comment-list"
          header={`${allReview.length} Reviews`}
          renderItem={<p>No review yet</p>}
        />
        :
        <List
            className="comment-list"
            header={`${allReview.length} Reviews`}
            itemLayout="horizontal"
            dataSource={allReview} 
            renderItem={review => (
            <li>
                <Comment
                actions={review.actions}
                author={review.detailProfile.name}
                avatar={
                <div>
                  <Avatar
                    icon={<UserOutlined />}
                    src={review.detailProfile.avatar}
                    alt="Han Solo"
                  />
                </div>
                }
                content={
                  <div>
                    <Rate count={10} tooltips={rateDesc} style={{fontSize:".8rem",display:"inline"}} disabled defaultValue={review.rating}/>
                    <span className="ant-rate-text">{rateDesc[review.rating - 1]}</span>
                    <p style={{display: "block", margin:0}} >{review.description}</p>
                  </div>
                }
                datetime={ <span>{moment().format('YYYY-MM-DD HH:mm:ss')}</span>}
                />
            </li>
            )}
        />}
        </ConfigProvider>
      </div>
    )
}
export default CommentList;