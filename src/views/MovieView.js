import React, { useEffect, useCallback} from 'react';
import {useSelector, useDispatch} from "react-redux";
import Overview from "./Overview";
import Reviews from './Reviews';
import Characters from './Characters';
import { Button, Space, Tabs, Tag} from 'antd';
import {movieDetails} from "../store/actions/moviedetail"
import {useParams} from "react-router-dom";
import RatingStar from "./RatingStar"
import '../assets/styles/MovieView.scss';

const MovieView = ()=> {

    const detail = useSelector(state => state.movieDetail.details);
    const genres = useSelector(state => state.movieDetail.genres);
    const dispatch = useDispatch();
    const {id} = useParams()
    
    const movieDetailCallback = useCallback(
        () => {
            dispatch(movieDetails(id))
        },
        [dispatch,id],
    )

    useEffect( ()=> {
        movieDetailCallback()
    },[movieDetailCallback])
        
    const { TabPane } = Tabs;
    return(
        <>
        <div className="movie-wrapper">
            <div className="movie-container" style={{backgroundImage:`url(https://image.tmdb.org/t/p/w300${detail.backdrop_path})`}}>
            {/* <img className="backdrop-image" src={`https://image.tmdb.org/t/p/w300${detail.backdrop_path}`} alt="Background Poster"/> */}
                <div className="movie-synopsis">
                    <div className="card-backrop-wrapper">
                        
                    </div>
                    <div className="poster">
                        <img className="poster-image" src={`https://image.tmdb.org/t/p/w300${detail.poster_path}`} alt="Movie Poster"/>
                    </div>
                    <div className="synopsis">
                        <h1>{detail.title}</h1>
                        <p>{genres.map(genre=> 
                            <Tag key={genre.id}>{genre.name}</Tag>)}
                        </p>
                        {detail.adult ? <div className="limit-age">
                            {detail.adult ? <img src={require("../assets/images/age-limit.svg")} alt="limit" className="limit-age"/> : ""}
                        </div> : ""}
                        <span>{detail.vote_average ? <RatingStar/> : <RatingStar/>}</span>
                        <p>{detail.overview}</p>
                        <div className="buttons">
                            <Space>
                                <Button className="primary-btn" type="primary">Watch Trailer</Button>
                                <Button className="second-btn" type="ghost">Add to Watchlist</Button>
                            </Space>
                        </div>
                    </div>
                </div>
            </div>

            
        </div>
        <div className="movie-menu">
            <Tabs defaultActiveKey="1">
                <TabPane tab="Overview" key="1">
                    <Overview/>
                </TabPane>
                <TabPane tab="Characters" key="2">
                    <Characters/>
                </TabPane>
                <TabPane tab="Review" key="3">
                    <Reviews/>
                </TabPane>
            </Tabs>
        </div>
    </>
    )
}

export default MovieView;