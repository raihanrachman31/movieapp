import React from "react";
import { Card , Empty} from "antd";
import '../assets/styles/MovieView.scss';
import {useSelector} from "react-redux";
const {Meta} = Card;

const Characters = ()=> {
    const characters = useSelector(state => state.movieDetail.characters);
    const character = characters.map(cast=>
        <div className="cast">
            <Card key={cast.id}
            className="character"
            hoverable
            cover={cast.profile_path ? <img alt="example" src={`https://image.tmdb.org/t/p/w300${cast.profile_path}`}/> :<Empty/>}
            >
                <Meta title={cast.name} description={`as ${cast.character}`} />
            </Card>
        </div>)
    return (
        
        <div className="characters-lists">
            {character}
        </div>
    )
}

export default Characters;