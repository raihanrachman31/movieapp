import React, { useEffect } from "react";
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import CommentForm from "./CommentForm"
import CommentList from "./CommentList";
import { movieDetails } from "../store/actions/moviedetail";

const Reviews = () => {
    const isAuthenticate = useSelector( state => state.auth.isAuthenticate );
    const { id } = useParams();
    const dispatch = useDispatch();

    useEffect(() => {
       if(isAuthenticate){
           dispatch(movieDetails(id))
       }
    }, [isAuthenticate,dispatch,id])

    return(
        <div className="reviews-wrapper">
            <CommentForm/>
            <CommentList/>
        </div>
    )
}

export default Reviews;