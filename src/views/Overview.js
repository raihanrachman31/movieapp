import React from "react";
import { Divider,Tag} from 'antd';
import '../assets/styles/MovieView.scss';
import {useSelector} from "react-redux";

const Overview = () => {
    const detail = useSelector(state => state.movieDetail.details);
    const companies = useSelector(state => state.movieDetail.companies);
    const budget = useSelector(state => state.movieDetail.budget);
    const revenue = useSelector(state => state.movieDetail.revenue);
    const genres = useSelector(state => state.movieDetail.genres);
    const duration = detail.runtime
    const changeHour =() => {
        var hours = Math.floor (duration/60)
        var minutes = duration % 60
        return `${hours} ${hours >1 ? "Hours" : "Hour"} ${minutes} ${minutes>1 ? "Minutes": "Minute"}`
    }
    const moneyConvert =(num,n)=> {
        var str = num.toString();
        let array = [];
        var chuncks =str.split('');
        var reverse_value = chuncks.reverse();
        let join = reverse_value.join('');
        for(let i = 0, len = join.length; i < len; i += n) {
            array.push(join.substr(i, n))
        }
        let arr_result = array.join('.')
        let arr_res = arr_result.split('');
        let rev_res = arr_res.reverse();
        let result = rev_res.join('')
        return result
    }
    return (
        <div className="divider">
            <Divider orientation="left" className="title">Synopsis</Divider>
            <p>{detail.overview}</p>
            <Divider orientation="left" className="title">Movie Info</Divider>
            <h3>Title : {detail.title}</h3>
            <h3>Genre : {genres.map(genre=> 
                <Tag key={genre.id}>{genre.name}</Tag>)}
            </h3>
            <h3>Movie Duration : {changeHour()}</h3>
            <h3>Release date : {detail.release_date}</h3>
            <h3>Production Company : {companies.map(company=> 
                <Tag key={company.id}>{company.name}</Tag>)}
            </h3>
                {budget ? <h3>Budget : USD {moneyConvert(budget,3)} </h3>: ""}
            {/* <h3>Revenue : {detail.revenue? detail.revenue: "-"}</h3> */}
            {revenue ? <h3>Revenue : USD {moneyConvert(revenue,3)} </h3>: ""}
        </div>
    )
}

export default Overview;