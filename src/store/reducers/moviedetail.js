import { GET_MOVIE_DETAIL_SUCCESS} from '../actions/types'
const initialState = {
    details: [],
    genres:[],
    characters:[],
    companies:[],
    budget:"",
    revenue:"",
    comments:[],
    myComment:{}
}

const movieDetail = (state = initialState, action) => {
    switch(action.type){
        case GET_MOVIE_DETAIL_SUCCESS:
            
            const currentUserId = localStorage.getItem('userId')
            console.log(action.payload.detailReviews.find( review => review.userId === parseInt(currentUserId)))
            return {
                ...state, 
                details:action.payload.movie, 
                genres:action.payload.movie.genres, 
                characters:action.payload.credits.cast,
                companies: action.payload.movie.production_companies,
                budget:action.payload.movie.budget,
                revenue:action.payload.movie.revenue,
                comments:action.payload.detailReviews,
                myComment: action.payload.detailReviews.find( review => review.userId === parseInt(currentUserId)) === undefined ? 
                
                    {
                      ...state,myComment:{}
                    }
                 :
                action.payload.detailReviews.find( review => review.userId === parseInt(currentUserId))

            }
        default:
            return{
                ...state
                }
    }
}

export default movieDetail;