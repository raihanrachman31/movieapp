import * as type from "../actions/types";

const initialState = {
  token: localStorage.getItem("token"),
  isAuthenticate: false,
  loading: false,
  updateStatus: "initial"
}

const auth = (state = initialState, action) => {
  switch (action.type) {
    default: 
      return {
        ...state
      }

    case type.PROFILE_LOADING_ACTIVE:
      return{
        ...state, loading: true
      }

    case type.PROFILE_LOADING_FINISHED:
      return{
        ...state, loading: false
      }
    
    case type.UPDATE_INITIAL:
      return{
        ...state, updateStatus: "initial"
      }

    case type.UPDATE_UPLOADING:
      return{
        ...state, updateStatus: "uploading",loading: true
      }

    case type.UPDATE_SUCCESS:
      return{
        ...state, updateStatus: "done",loading: false
      }
      
    case type.UPDATE_FAILED:
      return{
        ...state, updateStatus: "failed",loading: false
      }

    case type.LOGIN_SUCCESS : 
      return {
        ...state, 
        isAuthenticate: true
      }
    case type.LOGIN_FAILED : 
      return {
        ...state, 
        isAuthenticate: false,
        token: localStorage.removeItem("token"),
      }
    case "SIGNOUT" :
      localStorage.clear()
      return  {
        ...state, 
        isAuthenticate: false,
      }
    
  }
}

export default auth;