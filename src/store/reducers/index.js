import { combineReducers } from 'redux';
import auth from './auth';
import movies from './movies'
import movieDetail from './moviedetail'

export default combineReducers({
  auth,
  movies,
  movieDetail,
})