import { TOGGLE_SEARCH_FINISHED ,TOGGLE_SEARCH_ACTIVE ,GET_MOVIES_SUCCESS, SEARCH_BY_TITLE, FILTER_BY_GENRE, GET_ALL_MOVIES, GET_GENRES_SUCCESS, GET_MOVIES_ACTIVE, GET_MOVIES_FINISHED } from '../actions/types'
import { message } from 'antd'

const initialState = {
  movies: [],
  dataMovies:[],
  loading: false,
  genres:[],
  filterStatus: null,
  searchStatus: null,
  isSearch: false
}

const movies = (state = initialState, action ) => {
  switch(action.type) {

    case GET_MOVIES_ACTIVE:
      return {
        ...state, loading: true
      }
      
    case GET_MOVIES_FINISHED:
      return {
        ...state, loading: false
      }

    case GET_MOVIES_SUCCESS : 
      console.log("run")
      return {
        ...state, movies: action.payload, filterStatus: false, searchStatus: false
      }

    case SEARCH_BY_TITLE :
      console.log(action.payload)
      if(action.payload.total_results !== 0) {
        !action.page && message.info("search complete")
        return {
          ...state, movies: action.payload.results, filterStatus: false, searchStatus: action.status
        }
      } else {
       message.error("nothing found")
        return {
          ...state
        }
      }
      
    case FILTER_BY_GENRE : 
      return {
        ...state, movies: action.payload, filterStatus: action.status, searchStatus: false
      }
    
    case GET_ALL_MOVIES:
      return {
        ...state, movies: state.dataMovies
      }

    case GET_GENRES_SUCCESS:
    return {
      ...state, genres: action.payload
    }

    case TOGGLE_SEARCH_ACTIVE:
      return {
        ...state,isSearch: true
      }

    case TOGGLE_SEARCH_FINISHED:
      return {
        ...state,isSearch: false
      }

    default:
      return{
      ...state
    }
      
      
  }
}

export default movies;

