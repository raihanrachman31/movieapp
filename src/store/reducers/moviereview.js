
const initialValue = {
  myComments: [
    {
      author: "",
      avatar: '',
      content:"",
    }
  ],
  comments: [
    {
      author: "",
      avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
      content:"",
    }
  ]
} 

const movieReview = (state = initialValue,action ) =>{
  switch(action.type) {
    default: 
      return {
        ...state
      }
    
    case "ADD_COMMENT_SUCCESS" :
      return{
        ...state, myComments: action.payload
      }
  }
}
export default movieReview