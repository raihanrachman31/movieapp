import { message } from 'antd'
import axios from 'axios'
const token = localStorage.getItem('token')

export const submitReview = (data,id) => async () => {
  console.log("submit tun")
  try {
      await axios({
        method:"POST",
        url:`https://movie-review-apps-mp-team-g.herokuapp.com/api/v1/dmovies/${id}/review`,
        headers:{
          Authorization: token
        },
        data
      })
  } catch(error) {
    message.error("failed!")
  }
}

export const editReview = (data,id) => async()  => {
  try {
    await axios({
      method: "PUT",
      url: `https://movie-review-apps-mp-team-g.herokuapp.com/api/v1/dmovies/${id}/review`,
      headers:{
        Authorization: token
      },
      data
    })
  } catch(error) {
    message.error("failed!")
  }
}

export const deleteReview = movieId => async () => {
  try {
    const res = await axios({
      method: "DELETE",
      url: `https://movie-review-apps-mp-team-g.herokuapp.com/api/v1/dmovies/${movieId}/review`,
      headers:{
        Authorization: token
      }
      
    })
    console.log(res.status)
  } catch(error) {
    message.error("failed!")
  }
}