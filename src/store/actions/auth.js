import axios from 'axios'
import { message } from 'antd';
import { UPDATE_UPLOADING, UPDATE_FAILED ,UPDATE_SUCCESS ,LOGIN_SUCCESS, LOGIN_FAILED,PROFILE_LOADING_ACTIVE,PROFILE_LOADING_FINISHED } from '../actions/types'

export const register = ( data ) => async dispatch => {
  try {
    const res = await axios({
      method: "POST",
      url: "https://movie-review-apps-mp-team-g.herokuapp.com/api/v1/user/register",
      data
    })
    if(res.status === 201) {
      localStorage.setItem("userName",res.data.data.newUserProfile.name)
      localStorage.setItem("userAvatar",res.data.data.newUserProfile.avatar)
      localStorage.setItem("userId",res.data.data.newUserProfile.user_id)
      localStorage.setItem("token",res.data.data.token)
      message.info("Sign up success!")
      dispatch({
        type: LOGIN_SUCCESS
      })
    }
  } catch(error) {
    message.info(error.response.data.message)
    dispatch({
      type: LOGIN_FAILED
    })
  }
} 

export const login = ( data ) => async dispatch => {

  try {
    const res = await axios({
      method: "POST",
      url: "https://movie-review-apps-mp-team-g.herokuapp.com/api/v1/user/login ",
      data
    })
    if(res.status === 201) {
      localStorage.setItem("userName",res.data.data.User.profile.name)
      localStorage.setItem("userAvatar",res.data.data.User.profile.avatar)
      localStorage.setItem("userId",res.data.data.User.profile.user_id)
      localStorage.setItem("token",res.data.data.token)
      message.info("Sign in success!")
      await dispatch({
        type: LOGIN_SUCCESS,
      })
    }
  } catch(error) {
    message.info(error.response.data.message)
    await dispatch({
      type: LOGIN_FAILED,
    })
  }
}

export const validateToken = () => async dispatch => {
  dispatch({
    type:PROFILE_LOADING_ACTIVE
  })
  const token = localStorage.getItem("token")
  if(token){
    try {
      const res = await axios({
        method: "GET",
        url: "https://movie-review-apps-mp-team-g.herokuapp.com/api/v1/user",
        headers: {
          Authorization: token
        }
      })
      dispatch({
        type:PROFILE_LOADING_FINISHED
      })
      if(res.status === 200) {
        dispatch({
          type:LOGIN_SUCCESS
        })
      }
    } catch (error) {
      dispatch({
        type:PROFILE_LOADING_FINISHED
      })
    }
  }
}

export const updateProfile = (data,password) => async dispatch => {
  if(!!data){
    dispatch({
      type: UPDATE_UPLOADING
    })
    dispatch({
      type:PROFILE_LOADING_ACTIVE
    })
    const token = localStorage.getItem("token")
    try {
      const res = await axios({
        method: "PUT",
        url: "https://movie-review-apps-mp-team-g.herokuapp.com/api/v1/user",
        headers: {
          Authorization: token
        },
        data
      })
      if(res.status === 200) {
        message.info("Update success!")
        !!res.data.data.name && localStorage.setItem("userName",res.data.data.name)
        !!res.data.data.url && localStorage.setItem("userAvatar",res.data.data.url)
        dispatch({
          type: UPDATE_SUCCESS,
        })
      }
    } catch(error) {
      message.error("failed!")
      dispatch({
        type: UPDATE_FAILED,
      })
    }
  }
  

  if(!!password) {
    dispatch({
      type:PROFILE_LOADING_ACTIVE
    })
    const token = localStorage.getItem("token")
    try {
      const res = await axios({
        method: "PUT",
        url: "https://movie-review-apps-mp-team-g.herokuapp.com/api/v1/user/updatepassword",
        headers: {
          Authorization: token
        },
        data: {
          password
        }
      })
      if(res.status === 200) {
        message.success("Update success!")
        dispatch({
          type: UPDATE_SUCCESS,
        })
      }
    } catch(error) {
      message.failed("Failed!")
      dispatch({
        type: UPDATE_FAILED,
      })
    }
  }
}

