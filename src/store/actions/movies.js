import {GET_GENRES_SUCCESS,GET_MOVIES_SUCCESS,SEARCH_BY_TITLE, FILTER_BY_GENRE, GET_MOVIES_ACTIVE, GET_MOVIES_FINISHED, TOGGLE_SEARCH_ACTIVE } from '../actions/types'
import axios from 'axios'


export const getMovies = () => async (dispatch) => {
  try{
    const res = await axios({
      method: "GET",
      url: "https://movie-review-apps-mp-team-g.herokuapp.com/api/v1/dmovies/?page=1",
    })
    console.log(res.status)
    if (res.status === 200 ) {
      dispatch({
        type: GET_MOVIES_SUCCESS,
        payload: res.data.data.results
      })
    }
  } catch(error) {
  }
}

export const getMoviesBypage = page => async (dispatch) => {
  console.log("getdata running")
  dispatch({
    type: GET_MOVIES_ACTIVE,
  })
  try{
    const res = await axios({
      method: "GET",
      url: `https://movie-review-apps-mp-team-g.herokuapp.com/api/v1/dmovies/?page=${page}`,
    })
    console.log(res.data)
    if (res.status === 200 ) {
      dispatch({
        type: GET_MOVIES_SUCCESS,
        payload: res.data.data.results
      })
      dispatch({
        type: GET_MOVIES_FINISHED,
      })
    }
  } catch(error) {
    dispatch({
      type: GET_MOVIES_FINISHED,
    })
  }
}

export const getGenres = () => async (dispatch) => {
  try{
    const res = await axios({
      method: "GET",
      url: `https://api.themoviedb.org/3/genre/movie/list?api_key=9956fcd45e8edae4c064adef598f3ca5`,
    })
    console.log(res.status)
    if (res.status === 200 ) {
      dispatch({
        type: GET_GENRES_SUCCESS,
        payload: res.data.genres
      })
    }
  } catch(error) {
    console.log("get movies failed")
  }
}

export const searchMovie = (data,page) => async dispatch=> {
  dispatch({
    type: GET_MOVIES_ACTIVE,
  })
  try{
    const res = await axios({
      method: "GET",
      url: `https://movie-review-apps-mp-team-g.herokuapp.com/api/v1/dmovies/search/${data}?page=${page}`,
    })
    if (res.status === 200 ) {
      dispatch({
        type: SEARCH_BY_TITLE,
        payload: res.data.data,
        status: data,
        page: page
      })
      dispatch({
        type: TOGGLE_SEARCH_ACTIVE
      })
      dispatch({
        type: GET_MOVIES_FINISHED,
      })
    }
  } catch(error) {
    dispatch({
      type: GET_MOVIES_FINISHED,
    })
  }
}

export const filterMovieGenre = (genre,page) => async dispatch => {
  console.log("fileter", page, genre)
  try{
    const res = await axios({
      method: "GET",
      url: `https://movie-review-apps-mp-team-g.herokuapp.com/api/v1/dmovies/genre/${genre}?page=${page}`,
    })
    console.log(res.data)
    if (res.status === 200 ) {
      dispatch({
        type: FILTER_BY_GENRE,
        payload: res.data.data.results,
        status: genre
      })
    }
  } catch(error) {
    console.log("filter movies failed")
  }
}
