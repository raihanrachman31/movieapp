import {GET_MOVIE_DETAIL_SUCCESS } from '../actions/types'
import axios from 'axios'

export const movieDetails = (id) => async (dispatch) => {
  try{
    const res = await axios({
      method: "GET",
      url: `https://movie-review-apps-mp-team-g.herokuapp.com/api/v1/dmovies/${id}`,
    })
    console.log(res.data.data.detailReviews)
    
    if (res.status === 200 ) {
      dispatch({
        type: GET_MOVIE_DETAIL_SUCCESS,
        payload: res.data.data,
      })
    }
  } catch(error) {
    console.log("get movies failed")
  }
}
