export const LOGIN_SUCCESS = "LOGIN_SUCCESS"
export const LOGIN_FAILED = "LOGIN_FAILED"
export const PROFILE_LOADING_ACTIVE = "PROFILE_LOADING_ACTIVE"
export const PROFILE_LOADING_FINISHED = "PROFILE_LOADING_FINISHED"
export const UPDATE_SUCCESS = "UPDATE_SUCCESS"
export const UPDATE_FAILED = "UPDATE_FAILED"
export const UPDATE_UPLOADING = "UPDATE_UPLOADING"
export const UPDATE_INITIAL = "UPDATE_INITIAL"

export const GET_MOVIES_SUCCESS = "GET_MOVIES_SUCCESS"
export const GET_MOVIES_ACTIVE = "GET_MOVIES_ACTIVE"
export const GET_MOVIES_FINISHED = "GET_MOVIES_FINISHED"
export const GET_GENRES_SUCCESS = "GET_GENRES_SUCCESS"

export const SEARCH_BY_TITLE = "SEARCH_BY_TITLE"
export const FILTER_BY_GENRE = "FILTER_BY_GENRE"
export const GET_ALL_MOVIES = "GET_ALL_MOVIES"


export const GET_MOVIE_DETAIL_SUCCESS = "GET_MOVIE_DETAIL_SUCCESS"

export const GET_USER_REVIEW_SUCCESS = "GET_USER_REVIEW_SUCCESS"
export const GET_USER_REVIEW_FAILED = "GET_USER_REVIEW_FAILED"

export const TOGGLE_SEARCH_ACTIVE = "TOGGLE_SEARCH_ACTIVE"
export const TOGGLE_SEARCH_FINISHED = "TOGGLE_SEARCH_FINISHED"