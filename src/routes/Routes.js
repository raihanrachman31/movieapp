import React ,{useEffect, useState, useCallback}from 'react';
import { useDispatch} from 'react-redux';
import { Layout, Spin } from 'antd';
import HeaderSearch from '../layout/HeaderSearch';
import FooterContact from '../layout/FooterContact';
import MainView from '../views/MainView';
import { Route } from 'react-router-dom';
import '../assets/styles/Routes.scss';
import MovieView from '../views/MovieView';
import { validateToken } from "../store/actions/auth"
import { getMovies, getGenres } from "../store/actions/movies"

export default function Routes() {
  const { Header, Footer, Content } = Layout;
  const [pageLoading, setPageLoading] = useState(true)
  const dispatch = useDispatch()

  const loader = useCallback(
    async () => {
      if(localStorage.getItem("token")){
        await dispatch(validateToken())
      }
        await dispatch(getMovies())
        await dispatch(getGenres())
        setPageLoading(false)
    },
    [dispatch],
  )

  useEffect( () => {
    if(pageLoading){
      loader()
    }
   },[loader,pageLoading])

  return (
    <div>
      { pageLoading ?  <Spin style={{margin:"50vh 0"}} size="large" /> : 
        (<Layout className="layout">
          <Header className="header"><Route component={HeaderSearch}/></Header>
          <Content className="content">
            <Route path="/" component={MainView} exact/>
            <Route path="/:movie/:id" component={MovieView} exact/>
          </Content>
          <Footer className="footer"><FooterContact/></Footer>
        </Layout>)
      }
    </div>
  )
}
