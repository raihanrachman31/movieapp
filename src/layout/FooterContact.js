import React from "react";
import "../assets/styles/Footer.scss";
import { Tooltip } from 'antd';

const FooterContact = ()=> {
    return (
        <div style={{maxWidth:1152, margin: "0 auto"}} className="footer-wrapper">
            <div className="footer-container">
                <div className="footer-up">
                <div className="footer-description">
                    <img className="footer-logo" src={require('../assets/images/Movvix2.png')} alt="logo"/>
                    <p>Movvix is one of the biggest movie service provider app in the world.
                        Our office is in each other's home. Yes we work remotely. 
                        Founded in 2020 by Imam, Fadli, Raihan, Iqfar and Stacey.
                         Please support us by using our legal platform or download our application.</p>
                </div>
                <div className="footer-menu">
                    <ul>
                    <li><a href="/about-us">About us</a></li>
                        <li><a href="/blog">Blog</a></li>
                        <li><a href="/our-services">Our services</a></li>
                        <li>
                            <div className="career">
                                <a href="/career">Career</a>
                            <div className="badge-hiring">we're hiring</div>
                            </div>
                            </li>
                        <li><a href="/media-center">Media center</a></li>
                    </ul>
                </div>
                <div className="join-us">
                    <p>Download</p>
                    <div className="download-wrapper">
                        <img className="download-button" src={require('../assets/images/6.png')} alt="google-store"/>
                        <img className="download-button" src={require('../assets/images/3.png')} alt="apple-store"/>
                    </div>
                    <p>Sosial Media</p>
                    <div className="sosmed-wrapper">
                        <Tooltip placement="bottom" title="facebook"> <img className="medsos-icon" src={require('../assets/images/facebook.png')} alt="facebook"/></Tooltip>
                        <Tooltip placement="bottom" title="pinterest"><img className="medsos-icon" src={require('../assets/images/pinterest.png')} alt="pinterest"/></Tooltip>
                        <Tooltip placement="bottom" title="instagram"><img className="medsos-icon" src={require('../assets/images/instagram.png')} alt="instagram"/></Tooltip>
                    </div>
                </div>
                </div>
                <div className="footer-down">
                    <p>Copyright © 2020-2020 Watch App. All Rights Reserved</p>
                </div>
                
            </div>
        </div>
    )
}
export default FooterContact;