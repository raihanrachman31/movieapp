import React, { useState, useEffect } from 'react';
import {Row ,Col, Button, Form, Input, Avatar, Popover} from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux';
import { searchMovie } from '../store/actions/movies';
import { Link } from 'react-router-dom'
import '../assets/styles/HeaderSearch.scss';
import RegisterModal from '../components/RegisterModal'
import ProfileUpdateModal from '../components/ProfileUpdateModal'
import { TOGGLE_SEARCH_ACTIVE } from '../store/actions/types'

const HeaderSearch = (props) => {
  const { Search } = Input
  const [ registerModal,setRegisterModal] = useState(false)
  const [ profileModal,setProfileModal] = useState(false)
  const [ isLogin,setIsLogin ] = useState(false)
  const [search,setSearch] = useState("")
  const [userData, setUserData] = useState({
    name: localStorage.getItem("userName"),
    avatar: localStorage.getItem("userAvatar")
  })
  const isAuthenticate = useSelector( state => state.auth.isAuthenticate)
  const dispatch = useDispatch()

  const onChange = e => {
    setSearch(e.target.value)  
  }

  const onSearch = async () => {
    if(search === ""){
      return
    }
    dispatch(searchMovie(search))
    props.history.push("/")
  }

  const openRegisterModal = async ()=> {
    await setIsLogin(false)
    setRegisterModal(true)
  }

  const toggleIsLogin = async() => {
    await setRegisterModal(false)
    await setTimeout(()=>{ setIsLogin(!isLogin) }, 400) 
    setTimeout(()=>{ setRegisterModal(true) }, 400) 
  }

  const toggleSearch = () => {
    dispatch({type:TOGGLE_SEARCH_ACTIVE})
  }

    useEffect( ()=> {
      if(isAuthenticate){
        setUserData({
          name: localStorage.getItem("userName"),
          avatar: localStorage.getItem("userAvatar")
        })
      }
    },[isAuthenticate])

  const userPopOver = (
    <div>
      <p onClick={()=>setProfileModal(true)} style={{cursor:"pointer"}}>Profile</p>
      <ProfileUpdateModal
        setProfileModal={setProfileModal}
        profileModal={profileModal}
        setUserData={setUserData}
      />
      <p style={{cursor:"pointer"}}>Settings</p>
      <p onClick={()=>dispatch({type: "SIGNOUT"})} style={{cursor:"pointer"}}>Sign out</p>
    </div>

  )
  return (
    <>
      <Row className="header-content-wrapper"  align="middle">
        <Col span={2} >
          <Link to="/" className="logo-wrapper">
            <img src={require('../assets/images/Movvix.png')} alt="W i n t e r TV"/>
          </Link>
        </Col>
        <Col span={1} offset={1} >
          <Link className="navigation-text" to="/">
            Home
          </Link>
        </Col>
        <Col span={2} >
          <Link className="navigation-text" to="/">
            About
          </Link>
        </Col>
        <Col span={6} offset={isAuthenticate ? 6 : 9}>
          <Form onFinish={onSearch}>
            <Form.Item name="search" style={{margin:0}}>
              <Search onClick={toggleSearch} onSearch={onSearch} onChange={onChange} value={search} name="search" style={{textAlign:"center"}} placeholder="Search Movie"/>
            </Form.Item>
          </Form>
        </Col>

        <Col span={isAuthenticate ? 6 : 3} >        
          {isAuthenticate ? 
            (
              <>
                <Popover style={{width:100}} placement="bottomLeft" title={<span style={{textTransform: "capitalize", fontWeight:"bold"}}>{userData.name}</span>} content={userPopOver} trigger="hover">
                  <Avatar style={{margin: 10, cursor:"pointer"}} size="large" src={userData.avatar} icon={<UserOutlined />} />
                </Popover>
                <span style={{textTransform: "capitalize"}}>Welcome, {userData.name}</span>
              </>
            ) : <Button type="primary" onClick={openRegisterModal}>Sign Up</Button> 
          }
          <RegisterModal 
            toggleIsLogin={toggleIsLogin} 
            dispatch={dispatch}
            isLogin={isLogin}
            registerModal={registerModal}
            isAuthenticate={isAuthenticate}
            setRegisterModal={setRegisterModal}
          />
        </Col>
      </Row>
      </>
  )
}

export default HeaderSearch;