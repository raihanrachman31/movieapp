import React, { useState } from 'react'
import {Button, Form, Input, Modal, message, Upload, Divider, Typography} from 'antd';
import { updateProfile } from  '../store/actions/auth';
import { useSelector, useDispatch } from 'react-redux';
import { LoadingOutlined, PlusOutlined, EditOutlined} from '@ant-design/icons';
import store from '../store/index';
const { Text } = Typography

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
  console.log(reader.result)
}

function beforeUpload(file) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJpgOrPng && isLt2M;
}

const ProfileUpdateModal = props => {
  // component
  const [form] = Form.useForm()
  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
    labelAlign: "left"
  };

  //state
  const {setProfileModal, profileModal,setUserData} = props
  const [file,setFile] = useState(null)
  const [isChanged,setIsChanged] = useState({
    name:false,
    password:false
  })
  const [isEdit, setIsEdit] = useState({
    name: false,
    password:false
  })
  const [imgUpload, setImgUpload] = useState({
    loading:false,
    imgUrl: localStorage.getItem('userAvatar')
  })

  //store
  const loading = useSelector( state => state.auth.loading)
  const updateStatus = useSelector( state => state.auth.updateStatus)
  const dispatch = useDispatch()

  // registration
  const fieldNameChange = () => {
    setIsChanged({
      ...isChanged,name: true
    })
  }

  const fieldPasswordChange = () => {
    setIsChanged({
      ...isChanged,password: true
    })
  }

  const toggleEditName = ()=> {
    if(!!!isEdit.name){
      form.setFieldsValue({name: localStorage.getItem("userName")})
    } else {
      form.setFieldsValue({name:''})
    }
    setIsEdit({
      ...isEdit,name: !isEdit.name
    })
  }

  const toggleEditPassword = ()=> {
    !!isEdit.password && form.setFieldsValue({password:''})
    setIsEdit({
      ...isEdit,password: !isEdit.password
    })
  }

  const handleChangeCallback = () => {
    const status = store.getState().auth.updateStatus
    if(status === 'done') {
      console.log("done")
      getBase64(file, imgUrl =>
        setImgUpload({
          imgUrl,
          loading: false,
        }),
      );
      setUserData({
        name: localStorage.getItem("userName"),
        avatar: localStorage.getItem("userAvatar")
      })
      dispatch({type:"UPDATE_INITIAL"})      
    }
    if(status === 'failed') {
      setImgUpload({
        ...imgUpload,
        loading: false,
      })
      dispatch({type:"UPDATE_INITIAL"})      
    }
  }

  const handleChange = async (callback) => {
    const data = new FormData()
    data.append("image", file)
    if(updateStatus === 'initial') {
      console.log("uploading")
      setImgUpload({loading:true});
      await dispatch(updateProfile(data))
    }
    callback()
  };
   
  const submitUpdateName = async () => {
    const name = form.getFieldValue('name')
    const data = new FormData()
    if(!!!name){
      return;
    }
    data.append('name', name)
    await dispatch(updateProfile(data, false))
    toggleEditName()
    setIsChanged({
      ...isChanged,name:false
    })
    setUserData({
      name: localStorage.getItem("userName"),
      avatar: localStorage.getItem("userAvatar")
    })
  }
  const submitUpdatePassword = async () => {
    const password = form.getFieldValue('password')
    console.log(password)
    if(!!!password){
      return;
    }
    await dispatch(updateProfile(false, password))
    setIsChanged({
      ...isChanged,password:false
    })
    toggleEditPassword()
  }

  const closeModal =() => {
    setProfileModal(false)
    setIsChanged({
      name:false,
      password:false
    })
    setIsEdit({
      name: false,
      password:false
    })
    console.log("run")
  }

  const uploadButton = (
    <div>
      {imgUpload.loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div className="ant-upload-text">Upload</div>
    </div>)

  const { imgUrl } = imgUpload

  return (
    <Modal style={{textAlign:"left", transition: "all .4s ease"}}
      onCancel={closeModal}
      visible={profileModal}
      title = "My Profile"
      footer = {[
        <Button key="Back" type="primary" onClick={closeModal}>
          OK
        </Button>,
      ]}
    >
      <Form form={form} className="profile-modal-wrapper" {...layout}>
        <div className="uploader-wrapper">
          <Upload
            name="avatar"
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            beforeUpload={file => {beforeUpload(file); setFile(file)}}
            onChange={()=>handleChange(handleChangeCallback)}
          >
            {imgUrl ? <img src={imgUrl} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
          </Upload>
        </div>

        <div style={{marginBottom:"2rem"}}>
          <Divider plain orientation="left">
            <span style={{fontSize:"1rem"}}>Name</span>
            <EditOutlined 
              style={{marginLeft:8, color:"#5282A6"}}
              onClick={toggleEditName}
              />
          </Divider>
          {isEdit.name &&
            <div style={{display:"flex"}}>
              <Form.Item name="name"
                style={{marginBottom:0, flex:70}}
                validateStatus={loading && "validating"}
                hasFeedback
              >
                <Input autoFocus onPressEnter={submitUpdateName} onChange={fieldNameChange}
                  style={{marginLeft:"2rem", display:"inline"}}
                />
              </Form.Item>
              <div style={{marginBottom:0, flex:30,display:"flex",alignItems:"center",transform:"translateX(-50%)"}}>
                {isChanged.name &&
                  <Text
                    style={{margin:"0 8px",fontSize:".7rem",color:"#5282A6",cursor:"pointer"}}
                    onClick={submitUpdateName}
                    >
                    Save changes
                  </Text>
                }
                <Text
                    style={{margin:"0 8px",fontSize:".7rem",cursor:"pointer"}}
                    onClick={toggleEditName}
                    >
                    Cancel
                </Text>
              </div>
            </div>
          }
          {!isEdit.name &&
            <p style={{height:32,fontSize:".8rem",fontWeight:"bold" ,textAlign:"left", textTransform:"capitalize",marginLeft:"2.06rem"}}>{localStorage.getItem('userName')} </p>
          }
        </div>

        <div>
          <Divider plain orientation="left">
            <span style={{fontSize:"1rem"}}>Password</span>
            <EditOutlined 
              style={{marginLeft:8, color:"#5282A6"}}
              onClick={toggleEditPassword}
              />
          </Divider>
          {isEdit.password &&
            <div style={{display:"flex"}}>
              <Form.Item name="password"
                style={{marginBottom:0, flex:70}}
              >
                <Input autoFocus onPressEnter={submitUpdatePassword} onChange={fieldPasswordChange}
                  style={{marginLeft:"2rem", display:"inline"}}
                />
                
              </Form.Item>
              <div
                style={{marginBottom:0, flex:30,display:"flex",alignItems:"center",transform:"translateX(-50%)"}}
              >
                {isChanged.password &&
                  <Text
                    style={{margin:"0 8px",fontSize:".7rem",color:"#5282A6",cursor:"pointer"}}
                    onClick={submitUpdatePassword}
                    >
                    Save changes
                  </Text>
                }
                <Text
                    style={{margin:"0 8px",fontSize:".7rem",cursor:"pointer"}}
                    onClick={toggleEditPassword}
                    >
                    Cancel
                </Text>
              </div>
            </div>
          }
          {!isEdit.password &&
            <p style={{height:32,fontSize:".8rem",fontWeight:"bold" ,textAlign:"left", textTransform:"capitalize",marginLeft:"2.06rem"}}>********</p>
          }
        </div>
      </Form>
    </Modal>
  )
}

export default ProfileUpdateModal
