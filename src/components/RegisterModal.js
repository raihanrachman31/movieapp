import React, { useState, useEffect, useCallback} from 'react'
import {Button, Form, Input, Modal, Typography, Checkbox } from 'antd';
import { login, register } from  '../store/actions/auth';
import { useSelector } from 'react-redux';
import { UserOutlined, LockOutlined , MailOutlined} from '@ant-design/icons';

const RegisterModal = props => {
	// component
	const { Text, Title } = Typography
	const [form] = Form.useForm();
	const layout = {
		wrapperCol: { span: 16, offset: 4 },
	};

 //state
	const {setRegisterModal ,toggleIsLogin,dispatch,isLogin,registerModal} = props
	const [ isLoading,setIsLoading ] = useState(false)
	
	//store
	const isAuthenticate = useSelector( state => state.auth.isAuthenticate)

	//function
	const toggleLogin = async () => {
		form.resetFields()
		toggleIsLogin()
	}
	
	const closeModal = useCallback(
		() => {
			setRegisterModal(false)
		},[setRegisterModal],
	)

	const signOnFinish = async () => {
		setIsLoading(true)
		const dataRegister = {
			name:form.getFieldValue('name'),
			email: form.getFieldValue('email'),
			password: form.getFieldValue('password')
		}
		const dataLogin = {
			email: form.getFieldValue('email'),
			password: form.getFieldValue('password')
		}
		isLogin ? await dispatch(login(dataLogin)) : await dispatch(register(dataRegister))
		setIsLoading(false)
		form.resetFields()
	}

	useEffect( () => {
		if(isAuthenticate){
			closeModal()
		}
	},[isAuthenticate,closeModal])

	return (
		<div className="register-modal-wrapper">
				<Modal style={{width:400 ,transition: "all .4s ease"}}
				onCancel={()=>setRegisterModal(false)}
				visible={registerModal}
				title = {
					<>
					<img style={{
						width: "48px",
						height: "auto",
						borderRadius: "10px",
					}} 
						src={require('../assets/images/logoM2.png')} alt="Movvix"
					/>
					</>
				}
				footer = {false}
			>
				<Title style={{marginBottom:24, textAlign:"center"}} level={3}>{isLogin ? "Sign in" : "Sign up"}</Title>
				<Form onFinish={signOnFinish} name="normal_login" className="login-form" form={form} {...layout}>
					<Form.Item style={isLogin ? {display:"none"} :{}} name="name" rules={[{required:true, message:"please input your name!"}]}>
						<Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Fullname or Nickname"   onPressEnter={signOnFinish} name="name" style={isLogin ? {display:"none"} :{}} />
					</Form.Item>
					<Form.Item  name="email" rules={[{required: true, message: 'Please input your email!'}]}>
						<Input prefix={<MailOutlined className="site-form-item-icon" />} placeholder="Email" onPressEnter={signOnFinish} type="email" name="email"/>
					</Form.Item>
					<Form.Item name="password" rules={[{required:true, message: 'Please input your password!'}]}>
						<Input.Password prefix={<LockOutlined className="site-form-item-icon" />} placeholder="Password" onPressEnter={signOnFinish} name="password"  />
					</Form.Item>
					{isLogin ?
						<Form.Item>
							<Form.Item name="remember" valuePropName="checked" noStyle>
								<Checkbox>Remember me</Checkbox>
							</Form.Item>
							<a  className="login-form-forgot" href={"no"}>
								Forgot password
							</a>
						</Form.Item> :
						""
					}
					<Form.Item>
						<Button style={{marginBottom:8}} loading={isLoading} onClick={signOnFinish} type="primary" className="login-form-button" >
							{isLogin ? "Sign In" : "Sign Up"}
						</Button>
							Or <Text style= {{color: "#228DF8", cursor:"pointer"}} onClick={toggleLogin}>
										{isLogin ? "register now!" : "sign in"}
									</Text>
				</Form.Item>
				</Form>
			</Modal>
		</div>
	
	)
}

export default RegisterModal
