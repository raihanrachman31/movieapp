import React from 'react'
import { DeleteFilled, EditFilled } from '@ant-design/icons';
import { Comment, List, Tooltip } from 'antd';

const UserComment = ({displayComment, handleDelete, toggleEdit}) => (
  <List
    dataSource={displayComment}
    header={
      <div style={{display:"flex",justifyContent:"space-between", margin:0}}>
        My Review
        <div style= {{fontSize:"1rem", cursor:"pointer", margin: "0 8px"}}>
          <Tooltip style= {{margin: "0 8px"}} onClick={toggleEdit} title=" Edit my review ">
            <EditFilled style= {{margin: "0 8px"}} />
          </Tooltip>
          <Tooltip  onClick={handleDelete} title=" Delete ">
            <DeleteFilled style= {{margin: "0 8px"}}/>
          </Tooltip>
        </div>
      </div>
    }
		itemLayout="horizontal"
    renderItem={props => <Comment {...props} />}
  />
);

export default UserComment;
