import React from 'react';
import { Provider } from 'react-redux'
import store from './store/index'
import { BrowserRouter } from 'react-router-dom'
import './App.css';
import Routes from './routes/Routes';

function App() {
  return (
    <BrowserRouter>
      <Provider store = {store}>
        <div className="App">
          <Routes/>
        </div>
      </Provider>
    </BrowserRouter> 
    
  );
}

export default App;
